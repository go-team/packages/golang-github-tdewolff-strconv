Source: golang-github-tdewolff-strconv
Section: devel
Priority: extra
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Build-Depends: debhelper (>= 10),
               dh-golang,
               golang-any,
               golang-github-tdewolff-test-dev
Standards-Version: 4.0.0
Homepage: https://github.com/tdewolff/strconv
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-tdewolff-strconv
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-tdewolff-strconv.git
XS-Go-Import-Path: github.com/tdewolff/strconv
Testsuite: autopkgtest-pkg-go

Package: golang-github-tdewolff-strconv-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-tdewolff-test-dev
Description: Go string conversions for numeric types
 This package contains string conversion function and is written in Go.
 It is much alike the standard library's strconv package, but it is
 specifically tailored for the performance needs within the
 github.com/tdewolff/minify package.
 .
 For example, the floating-point to string conversion function is
 approximately twice as fast as the standard library, but it is not
 as precise.
